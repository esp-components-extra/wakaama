# SPDX-License-Identifier: BSD-2-Clause-Patent
#
# Copyright (c) 2020 Albert Krenz <albert.krenz@mailbox.org>

function(enable_cxx_compiler_flag flag)
  string(FIND "${CMAKE_CXX_FLAGS}" "${flag}" flag_set)
  if(flag_set EQUAL -1)
    # message(STATUS "Enable flag ${flag}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${flag}" PARENT_SCOPE)
  endif()
endfunction()

function(enable_c_compiler_flag flag)
  string(FIND "${CMAKE_C_FLAGS}" "${flag}" flag_set)
  if(flag_set EQUAL -1)
    # message(STATUS "Enable flag ${flag}")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${flag}" PARENT_SCOPE)
  endif()
endfunction()