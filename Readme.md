<!-- SPDX-License-Identifier: CC-BY-4.0-->
<!-- This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA. -->

<!-- Copyright (c) 2020 Albert Krenz <albert.krenz@mailbox.org> -->

# ESP wakaama/liblwm2m wrapper
This is a Wrapper for wakaama/liblwm2m (https://github.com/eclipse/wakaama) to be used as Component in Espressif IDF (https://github.com/espressif/esp-idf)

# How to include wakaama into your project

To include this component into your project create a components directory under your project folder

    <your-project-path>
    ├── CMakeLists.txt
    ├── components
    │   └── wakaama
    ├── main
    │   ├── CMakeLists.txt
    │   └── main.c

Add the wrapper as submodule to git via

    # git submodule add components/wakaama git@gitlab.com:esp-components-extra/wakaama.git
    # git submodule init && git submodule update --resursive

Or clone this repository directly into the components folder

    # cd components
    # git clone --recursive git@gitlab.com:esp-components-extra/wakaama.git

# How to use this component
To use this component you need to specify the `IDF_EXTRA_COMPONENT_DIRS` variable within your main CMakeLists.txt before including `$IDF_PATH/tools/cmake/project.cmake` or `IDF_PATH}/tools/cmake/idf_functions.cmake`. Afterwards you can use wakaama as any other component from ESP-IDF.

An example CMakeLists could look like that

``` cmake
    cmake_minimum_required(VERSION 3.5)

    set(IDF_EXTRA_COMPONENT_DIRS ${CMAKE_SOURCE_DIR}/components)
    set(IDF_COMPONENTS freertos spi_flash bootloader esptool_py wakaama main log)
    include($ENV{IDF_PATH}/tools/cmake/project.cmake)
    project(wakaama-client)
```

with the following project structure

    <your-project-folder>
    ├── CMakeLists.txt
    ├── components
    │   └── wakaama
    ├── main
    │   ├── CMakeLists.txt
    │   ├── main.c
    └── sdkconfig


# License
This Wrapper is licensed under BSD-2-Clause Plus Patent License. For the full License text see the LICENSE file