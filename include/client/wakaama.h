//SPDX-License-Identifier: BSD-2-Clause-Patent
//
//Copyright (c) 2020 Albert Krenz <albert.krenz@mailbox.org>

#ifndef WAKAAMA_H_
#define WAKAAMA_H_

#define LWM2M_CLIENT_MODE
#include "../../wakaama/core/liblwm2m.h"

#endif /* WAKAAMA_H_ */